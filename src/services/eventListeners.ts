import {renderMovies, addFavoriteMovies} from '../components/movies';
import {getUpcomingMovies, getTopRatedMovies, getPopularMovies,
    searchMovies, getMovieDetails} from './moviesRequest';
import {IMovies, IMovie} from '../types/interfaces';
import {saveMovie, deleteMovie, getMovies} from '../helpers/localStorageHelper';

export function addEventListeners():void {
    let renderedMovies: string;
    let page: number = 1;

    const upcomingBtn = <HTMLElement>document.getElementById('upcoming');
    upcomingBtn.addEventListener('click', async () => {
        const upcomingMovies: IMovies = await getUpcomingMovies();
        renderedMovies = 'upcoming';
        renderMovies(upcomingMovies, true);
    })

    const topRatedBtn = <HTMLElement>document.getElementById('top_rated');
    topRatedBtn.addEventListener('click', async () => {
        const topRatedMovies: IMovies = await getTopRatedMovies();
        renderedMovies = 'topRated';
        renderMovies(topRatedMovies, true);
    })

    const popularBtn = <HTMLElement>document.getElementById('popular');
    popularBtn.addEventListener('click', async () => {
        const popularMovies: IMovies = await getPopularMovies();
        renderedMovies = 'popular';
        renderMovies(popularMovies, true);
    })

    const form = <HTMLElement>document.forms[0];
    const input = <HTMLInputElement>document.getElementById('search');
    const searchBtn = <HTMLElement>document.getElementById('submit');
    searchBtn.addEventListener('click', async  () => {
        const params = `&query=${input.value}`;
        const searchedMovied: IMovies = await searchMovies(params);
        renderMovies(searchedMovied, true);
    })
    form.addEventListener('submit', async  () => {
        const params = `&query=${input.value}`;
        const searchedMovied: IMovies = await searchMovies(params);
        renderMovies(searchedMovied, true);
    })

    const filmContainer = <HTMLElement>document.getElementById('film-container');
    document.addEventListener('click', (event) => {
        event.stopPropagation();
        const target = <HTMLElement>event.target;
        
        if (target.tagName !== 'path') return;
        const card = <HTMLElement>target.closest('.card.shadow-sm');
        const id = Number(card.getAttribute('id'));
        
        if (target.parentElement!.getAttribute('fill') === 'red') {
            target.parentElement!.setAttribute('fill', '#ff000078');
            deleteMovie(id);
        } else {
            target.parentElement!.setAttribute('fill', 'red');
            saveMovie(id);
        }
        
        addFavoriteMovies();
    })

    const loadMoreBtn = <HTMLElement>document.getElementById('load-more');
    loadMoreBtn.addEventListener('click', async () => {
        page += 1;
        const params: string = `&page=${page}`;
        let movies: IMovies;

        switch (renderedMovies) {
            case 'upcoming':
                movies = await getUpcomingMovies(params);
                break;
            case 'topRated':
                movies = await getTopRatedMovies(params);
                break;
            default:
                movies = await getPopularMovies(params);
        }

        renderMovies(movies);
    })


}