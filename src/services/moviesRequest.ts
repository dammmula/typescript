import {makeRequest} from '../helpers/requestHelper';

export const getPopularMovies = async(params?: string) => {
    const path: string = '/movie/popular';
    const popularMovies = await makeRequest(path, params);
    return popularMovies;
}

export const getUpcomingMovies = async(params?: string) => {
    const path: string = '/movie/upcoming';
    const upcomingMovies = await makeRequest(path, params);
    return upcomingMovies;
}

export const getTopRatedMovies = async(params?: string) => {
    const path: string = '/movie/top_rated';
    const topRatedMovies = await makeRequest(path, params);
    return topRatedMovies;
}

export const searchMovies = async(params?: string) => {
    const path: string = '/search/movie';
    const searchedMovies = await makeRequest(path, params);
    return searchedMovies;
}

export const getMovieDetails = async(id: number) => {
    const path: string = `/movie/${id}`;
    const movieDetails = await makeRequest(path);
    return movieDetails;
}