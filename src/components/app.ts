import {getPopularMovies, getUpcomingMovies, getTopRatedMovies, searchMovies} from '../services/moviesRequest';
import {renderMovies, renderRandomMovie, addFavoriteMovies} from './movies';
import {IMovies} from '../types/interfaces';
import {addEventListeners} from '../services/eventListeners';

class App {
    
    async startApp(): Promise<void> {
        try {
            const rootElement = <HTMLElement>document.getElementById('root');
            
            const popularMovies: IMovies | undefined = await getPopularMovies();    

            if (popularMovies) {
                renderRandomMovie(popularMovies);
                renderMovies(popularMovies);
                addEventListeners();
                addFavoriteMovies()
            } else {
                rootElement.innerText = 'Failed to load data';
            }
            
        } catch (e) {
            console.error(e);
        }
    }

}

export default App;