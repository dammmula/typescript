import {IMovies, IMovie} from '../types/interfaces';
import {getPopularMovies, getUpcomingMovies, getTopRatedMovies, searchMovies, getMovieDetails} from '../services/moviesRequest';
import {isSaved} from '../helpers/localStorageHelper';
import {getMovies} from '../helpers/localStorageHelper';

type Pair<T,K> = [T,K];
type Pairs<T,K> = Pair<T,K>[];
const baseUrl: string = 'https://image.tmdb.org/t/p/w500';


export function renderMovies(movies: IMovies, replace?: boolean): void {

    const filmContainer = <HTMLElement>document.getElementById('film-container');
    let movieCards: HTMLElement[] = [];

    for (let movie of movies.results) {
        const movieCard: HTMLElement = createElement('div', 'col-lg-3 col-md-4 col-12 p-2');
        movieCard.append(createMovieCard(movie))
        movieCards.push(movieCard);
    }

    if (replace) {
        filmContainer.innerHTML = '';
    }

    filmContainer.append(...movieCards);
}


function createMovieCard(movie: IMovie): HTMLElement {
    const card: HTMLElement = createElement('div', 'card shadow-sm');
    const img: HTMLElement = createElement('img', '', [['src', `${baseUrl}${movie.poster_path}`]]);
    const fill: string = isSaved(movie.id) ? 'red' : '#ff000078';
    const svg: string = `<svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill=${fill} width="50" height="50" class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22"><path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/></svg>`;
    const cardBody: string = `<div class="card-body"">
                                    <p class="card-text truncate">
                                        ${movie.overview}
                                    </p>
                                    <div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                        <small class="text-muted">${movie.release_date}</small>
                                    </div>
                                </div>`;
    
    card.setAttribute('id', `${movie.id}`);  
    card.append(img);
    card.insertAdjacentHTML('beforeend', svg);
    card.insertAdjacentHTML('beforeend', cardBody);
    
    return card;
}

export function createElement(tagName: string, className: string, attributes?: Pairs<string, string>): HTMLElement {
    const element = <HTMLElement>document.createElement(tagName);

    if (className) {
        const classNames: string[] = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (attributes) {
        for (let tuple of attributes) {
            element.setAttribute(tuple[0], tuple[1]);
        }
    }

    return element;
}

export function renderRandomMovie(movies: IMovies) {
    const section = <HTMLElement>document.getElementById('random-movie');
    const randomInt: number = Math.floor(Math.random() * (movies.results.length)) + 1;
    const movie: IMovie = movies.results[randomInt];
    const element: string = ` <div class="row py-lg-5">
                    <div
                        class="col-lg-6 col-md-8 mx-auto"
                        style="background-color: #2525254f"
                    >
                        <h1 id="random-movie-name" class="fw-light text-light">${movie.title}</h1>
                        <p id="random-movie-description" class="lead text-white">
                            ${movie.overview}
                        </p>
                    </div>`;

    section.style.background = `url("${baseUrl}${movie.poster_path}")`;
    section.insertAdjacentHTML('beforeend', element);
}

export async function addFavoriteMovies() {
    const movies = await getMovies();
    const container = <HTMLElement>document.getElementById('favorite-movies');
    container.innerHTML = '';

    for (let movie of movies) {
        const movieCard: HTMLElement = createElement('div', 'col-12 p-2');
        const movieDetails: IMovie = await getMovieDetails(movie);
        movieCard.append(createMovieCard(movieDetails));
        container.append(movieCard);
    }
}