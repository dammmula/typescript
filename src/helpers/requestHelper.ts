const apiUrl: string = 'https://api.themoviedb.org/3';
const apiKey: string = '?api_key=406e78ec00942378e84feee40266e786';

export const makeRequest = async (path: string, params: string = '') => {
    try {
        const url: string = getUrl(path, params);

        const res: Response = await fetch(url, {
            method: 'GET',
            headers: { "Content-Type": "application/json" }
        });

        const dataObj = await res.json();

        if(!res.ok) {
            alert(`${dataObj.message}`);
        }

        return dataObj;
    } catch (err) {
        console.error(err);
    }
}

function getUrl(path: string, params: string): string {
    return `${apiUrl}${path}${apiKey}${params}`;
}

