import {IMovie} from '../types/interfaces';

export function saveMovie(id: number):void {
    try {
        const item: string|null = localStorage.getItem('favoriteMovies');
        let favoriteMovies: number[];

        if (item) {
            favoriteMovies = JSON.parse(item);
        } else {
            favoriteMovies = [];
        }

        favoriteMovies.push(id);
        localStorage.setItem('favoriteMovies', JSON.stringify(favoriteMovies));
    } catch (error) {
        if (error === 'QUOTA_EXCEEDED_ERR') {
            alert('Memory is full. Cannot save');
        }
        console.warn(error);;
    }
}

export function deleteMovie(id: number):void {
    try {
        const item: string|null = localStorage.getItem('favoriteMovies');
        if (!item) return;
        
        const favoriteMovies = JSON.parse(item).filter((item: number) => item !== id);
        localStorage.setItem('favoriteMovies', JSON.stringify(favoriteMovies));
        
    } catch (error) {
        console.warn(error);
    }
}

export function isSaved(id: number): boolean {
    const item: string|null = localStorage.getItem('favoriteMovies');
    if (!item) return false;

    const found: boolean = JSON.parse(item).find((item: number) => item == id);
    
    if (found) return true;
    return false;
}

export function getMovies(): number[] {
    const item: string|null = localStorage.getItem('favoriteMovies');
    
    if (item) {
        return JSON.parse(item);
    }
    
    return [];
}