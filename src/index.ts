import App from './components/app';

export async function render(): Promise<void> {
    const app = new App();
    app.startApp();
    // TODO render your app here
}
