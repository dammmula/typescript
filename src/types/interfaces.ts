export interface IMovies {
    page: number,
    results: IMovie[];
    total_pages: number;
    total_results: number;
}

export interface IMovie {
    id: number,
    title: string;
    overview: string;
    poster_path: string;
    release_date: string;
}